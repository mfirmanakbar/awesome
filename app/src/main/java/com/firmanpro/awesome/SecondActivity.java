package com.firmanpro.awesome;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        MainActivity.sample_val = "value from second activity";

    }

    public void onClickPopup(View view){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(SecondActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View alertView = inflater.inflate(R.layout.popup_desc, null);
        alertDialog.setView(alertView);

        final AlertDialog show = alertDialog.show();
        Button alertButton = (Button) alertView.findViewById(R.id.btn_test);

        alertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.dismiss();
            }
        });
    }

}
