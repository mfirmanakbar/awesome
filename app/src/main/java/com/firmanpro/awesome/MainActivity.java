package com.firmanpro.awesome;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.net.Inet4Address;

public class MainActivity extends AppCompatActivity {

    private String TAG = "act_main";
    private TextView txtSample;
    public static String sample_val = "Hello";

    /**setup view UI here*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtSample = (TextView)findViewById(R.id.txtSample);

    }

    public void onClickTOSecondAct(View view){
        startActivity(new Intent(MainActivity.this, SecondActivity.class));
    }

    /**setup business process*/
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "call onStart");
        //txtSample.setText("on start");
        txtSample.setText(sample_val);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "call onResume");
        //txtSample.setText(sample_val);
    }

    /**called when open aother activity in front this activity*/
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "call onPause");
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Log.d(TAG, "call onWindowFocusChanged");
    }

    /**called when apps close or move to other activity*/
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "call onStop");
    }

    /**called when the app is close*/
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "call onDestroy");
    }
}
